import board
import busio
import adafruit_pca9685


i2c = busio.I2C(board.SCL, board.SDA)
pca = adafruit_pca9685.PCA9685(i2c)

pca.frequency = 24

led0 = pca.channels[0]


direction = 0xf
brightness = 0
run = True
while run:
	brightness += direction
	if brightness >= 0xffff:
		brightness = 0xffff
		direction = -0xf
	elif brightness <= 0x0000:
		brightness = 0x0000
		direction = 0xf
		
	try:
		led0.duty_cycle = brightness
	except KeyboardInterrupt:
		run = False
