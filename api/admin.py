from django.contrib import admin

from .models import ColorGroup, PositionGroup, LED

admin.site.register(ColorGroup)
admin.site.register(PositionGroup)
admin.site.register(LED)
