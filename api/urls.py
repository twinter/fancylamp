from django.urls import path

from . import views

urlpatterns = [
	path('brightness/set/<int:brightness>', views.set_all, name='set_all'),
	path('brightness/set/<int:brightness>/for/<str:name>', views.set_single, name='set_single'),
]
