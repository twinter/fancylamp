from api.models import LED
from api.helpers import set_led_state, limit, generate_response


# TODO: restrict views to certain HTTP methods

def set_all(request, brightness):
	try:
		brightness = limit(brightness, 0, 4095)
	except ValueError as e:
		return generate_response('brightness: {}'.format(e), False)
	
	leds = LED.objects.all()
	brightness_dict = {}
	for l in leds:
		brightness_dict[l] = brightness
	set_led_state(brightness_dict)
	
	return generate_response('all LEDs set to {:x}'.format(brightness))


def set_single(request, name: str, brightness: int):
	try:
		brightness = limit(brightness, 0, 4095)
	except ValueError as e:
		return generate_response('brightness: {}'.format(e), False)
	
	led = LED.objects.get(name=name)
	set_led_state({led: brightness})
	
	return generate_response('LED with ID {} set to {:x}'.format(name, brightness))
