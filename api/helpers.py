import json
import operator
from typing import Union, Dict

from django.http import HttpResponse
from django.db import transaction
import board
import busio
import adafruit_pca9685

from api.models import LED

Number = Union[int, float]


@transaction.atomic
def _save_led_state(leds: Dict[LED, int]):
	db_leds = LED.objects.all()
	for l in db_leds:
		if l in leds:
			l.brightness = leds[l]
			l.save()


def set_led_state(leds: Dict[LED, int]):
	"""
	Set the LEDs as specified by the dict.
	
	:param leds: a dict specifying the brightness for each LED, format: {LED: brightness}
	"""
	
	i2c = busio.I2C(board.SCL, board.SDA)
	
	# this returns a list, not a dict. don't convert to dict as this screws up the sorting
	leds_sorted = sorted(leds.items(), key=operator.itemgetter(0))
	last_address = None
	pca = None
	for led, brightness in leds_sorted:
		if last_address != led.address:
			if pca is not None:
				pca.deinit()
			pca = adafruit_pca9685.PCA9685(i2c, address=led.address)
			pca.frequency = 1600
			last_address = led.address
			
		pca.channels[led.channel].duty_cycle = brightness * 0x10
	if pca is not None:
		pca.deinit()
		
	_save_led_state(leds)


def limit(v: Number, v_min: Number, v_max: Number, raise_exception=True) -> Number:
	"""
	Limit the given value to a given range (inclusive) either by raising an
	Exception or by silently adjusting the value.
	
	:param v: the value
	:param v_min: the minimum of the range (inclusive)
	:param v_max: the maximum of the range (inclusive)
	:param raise_exception: raises an Exception if True, adjusts the value if False
	:return: v within the given limits
	"""
	
	assert v_min <= v_max
	
	if raise_exception:
		if v < v_min or v > v_max:
			raise ValueError('value has to be in range [{}, {}]'.format(v_min, v_max))
	else:
		v = max(v, v_min)
		v = min(v, v_max)
	return v


def generate_response(message: str, success=True) -> HttpResponse:
	"""
	Generates a JSON formatted HttpResponse object with a custom status and message.
	
	:param message: the message to deliver to the client
	:param success: True if the message reports a success, False otherwise
	:return: the complete HttpResponse
	"""
	
	response = {
		'status':  'success' if success else 'error',
		'message': message
	}
	response_json = json.dumps(response)
	return HttpResponse(response_json)
