import functools

from django.db import models
from django.core.validators import MaxValueValidator


class ColorGroup(models.Model):
	name = models.CharField(max_length=200, primary_key=True)
	ctemp = models.IntegerField(verbose_name='color temperature')
	
	def __str__(self):
		return self.name
	
	
class PositionGroup(models.Model):
	name = models.CharField(max_length=200, primary_key=True)
	size_x = models.FloatField(verbose_name='width')
	size_y = models.FloatField(verbose_name='height')
	pos_x = models.FloatField(verbose_name='x coordinate')
	pos_y = models.FloatField(verbose_name='y coordinate')
	
	def __str__(self):
		return self.name


@functools.total_ordering
class LED(models.Model):
	name = models.CharField(max_length=200, primary_key=True)
	address = models.PositiveSmallIntegerField(default=0x40)
	channel = models.PositiveSmallIntegerField(validators=[MaxValueValidator(15)])
	color_group = models.ForeignKey(to=ColorGroup, on_delete=models.CASCADE)
	position_group = models.ManyToManyField(to=PositionGroup, blank=True)
	brightness = models.PositiveSmallIntegerField(validators=[MaxValueValidator(4095)], default=0)
	
	def __str__(self):
		return self.name
	
	def __eq__(self, other):
		return self.address == other.address and self.channel == other.channel
	
	def __lt__(self, other):
		if self.address == other.address:
			return self.channel < other.channel
		else:
			return self.address < other.address
