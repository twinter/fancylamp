# Generated by Django 2.1.1 on 2018-09-29 18:00

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ColorGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('ctemp', models.IntegerField(verbose_name='color temperature')),
            ],
        ),
        migrations.CreateModel(
            name='LED',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('address', models.PositiveSmallIntegerField(default=64)),
                ('channel', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(15)])),
                ('brightness', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(4095)])),
                ('color_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.ColorGroup')),
            ],
        ),
        migrations.CreateModel(
            name='PositionGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('size_x', models.FloatField(verbose_name='width')),
                ('size_y', models.FloatField(verbose_name='height')),
                ('pos_x', models.FloatField(verbose_name='x coordinate')),
                ('pos_y', models.FloatField(verbose_name='y coordinate')),
            ],
        ),
        migrations.AddField(
            model_name='led',
            name='position_group',
            field=models.ManyToManyField(to='api.PositionGroup'),
        ),
    ]
